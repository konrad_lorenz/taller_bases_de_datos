# Taller de Bases de Datos - Análisis Bibliográfico 

## Objetivo
Los estudiantes realizarán un análisis bibliográfico de no menos de 100 artículos sobre un tema específico de su elección dentro de las áreas de tecnología, ingeniería, ciencia de datos, aprendizaje automático (ML), inteligencia artificial (IA), etc. Deberán analizar títulos, abstracts, palabras clave, fuentes y autores, y entregar un informe final que refleje su comprensión y análisis crítico del tema.

## Requisitos
- **Selección del Tema**: Cada estudiante escogerá un tema específico dentro de las áreas propuestas.
- **Búsqueda Inicial**: Realizar una búsqueda inicial para recopilar un mínimo de 100 artículos relacionados con el tema elegido.

## Metodología
1. **Construcción de la Query Inicial**: Los estudiantes desarrollarán una consulta inicial para la búsqueda de artículos en bases de datos académicas como IEEE Xplore, PubMed, Scopus, etc. Deberán documentar esta consulta como parte del informe final.

2. **Análisis de Datos Recopilados**:
   - **Títulos**: Identificar temas comunes y tendencias emergentes.
   - **Abstracts**: Resumir los enfoques principales y hallazgos de los estudios.
   - **Palabras Clave**: Analizar la frecuencia y la relevancia de las palabras clave para identificar subtemas y tendencias.
   - **Fuentes**: Evaluar las revistas o conferencias más citadas y su impacto en el campo.
   - **Autores**: Identificar los autores más prolíficos y sus contribuciones al área de estudio.

3. **Revisión y Query Resultante**: Ajustar la consulta inicial basada en los hallazgos preliminares para refinar la búsqueda de artículos más relevantes o recientes.

4. **Análisis Comparativo**: Comparar los resultados obtenidos en diferentes periodos (por ejemplo, últimos 5 años versus los 5 años anteriores) para identificar cambios en las tendencias y enfoques de investigación.

5. **Generación de Mapas de Categorías Gramaticales**: Utilizar herramientas de NLP para analizar la estructura gramatical de los abstracts y títulos, y generar visualizaciones que muestren las categorías gramaticales más frecuentes.

## Entrega del Informe
El informe final deberá incluir:
- **Descripción del Tema y Justificación de la Elección**.
- **Metodología Aplicada**, incluyendo la construcción de las queries y los métodos de análisis.
- **Resultados del Análisis** de títulos, abstracts, palabras clave, fuentes, y autores.
- **Discusión de los Resultados**, interpretando los cambios y tendencias observadas.
- **Conclusiones y Recomendaciones** para futuras investigaciones.
- **Apéndices** con gráficos, tablas y mapas de categorías gramaticales.

## Evaluación
La evaluación se basará en la exhaustividad del análisis, la claridad en la presentación y argumentación en el informe, y la aplicación efectiva de la metodología propuesta por los autores del documento guía.

[Repositorio Guía para Análisis Bibliográfico](https://gitlab.com/konrad_lorenz/bibliographic_analisis)
